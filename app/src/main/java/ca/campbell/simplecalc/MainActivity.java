package ca.campbell.simplecalc;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

//  TODO: add a field to input a 2nd number, get the input and use it in calculations
//  TODO: the inputType attribute forces a number keyboard, don't use it on the second field so you can see the difference

//  TODO: add buttons & methods for subtract, multiply, divide

//  TODO: add input validation: no divide by zero
//  TODO: input validation: set text to show error when it occurs

//  TODO: add a clear button that will clear the result & input fields

//  TODO: the hint for the result widget is hard coded, put it in the strings file

public class MainActivity extends Activity {
    EditText etNumber1,etNumber2;
    TextView addResult, subResult,mltpResult, divResult;
    double num1, num2;

    private void takeInput(){
        num1 = Double.parseDouble(etNumber1.getText().toString());
        num2 = Double.parseDouble(etNumber2.getText().toString());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // get a handle on the text fields
        etNumber1 = (EditText) findViewById(R.id.num1);
        etNumber2 = (EditText) findViewById(R.id.num2);
        addResult = (TextView) findViewById(R.id.addResult);
        subResult = (TextView) findViewById(R.id.subResult);
        mltpResult = (TextView) findViewById(R.id.mltpResult);
        divResult = (TextView) findViewById(R.id.divResult);
    }  //onCreate()

    // TODO: replace with code that adds the two input numbers
    public void calcResult(View v) {
        takeInput();
        addResult.setText(Double.toString(num1+num2));
    }  //addNums()

    public void subResult(View v){
        takeInput();
        subResult.setText(Double.toString(num1-num2));
    }

    public void mltpResult(View v){
        takeInput();
        mltpResult.setText(Double.toString(num1*num2));
    }

    public void divResult(View v){
        takeInput();
        if(num2 == 0)
            divResult.setText("Division by zero is not valid!");
        else
            divResult.setText(Double.toString(num1/num2));
    }

    public void Clear(View v){
        addResult.setText("");
    }

}